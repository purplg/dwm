/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const unsigned int gappx     = 3;        /* gap pixel between windows */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=12";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const int T_CODE = 1 << 1;
static const int M_CODE = 1;
static const int T_CHAT = 1 << 0;
static const int M_CHAT = 1;
static const int T_GAME = 1 << 1;
static const int M_GAME = 0;

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class              instance        title                  tags mask   isfloating   isterminal   noswallow   monitor */
    // Editors
	{ "jetbrains-rider",  NULL,           NULL,                  T_CODE,     0,           0,           0,          M_CODE },
	{ "Emacs",            NULL,           NULL,                  T_CODE,     0,           0,           0,          M_CODE },
	{ "code-oss",         NULL,           NULL,                  T_CODE,     0,           0,           0,          M_CODE },

	// Browsers
	{ "firefox",          NULL,           NULL,                       0,     0,           0,           0,          -1 },

	// Terminals
	{ "Alacritty",        NULL,           NULL,                       0,     0,           1,           0,          -1 },

	// Communication
	{ "discord",          NULL,           NULL,                  T_CHAT,     0,           0,           0,          M_CHAT },
	{ "TelegramDesktop",  NULL,           NULL,                  T_CHAT,     0,           0,           0,          M_CHAT },

	// Launchers
	{ "Lutris",           NULL,           NULL,                       0,     1,           0,           0,           0 },
	{ "steam_proton",     NULL,           NULL,                       0,     1,           0,           1,           0 },
	{ "Steam",            NULL,           NULL,                       0,     0,           0,           1,           0 },

	// Big apps
	{ "UnityHub",         NULL,           NULL,                  1 << 1,     1,           0,           0,           0 },
	{ "Blender",          NULL,           NULL,                  1 << 2,     0,           0,           0,           0 },
	{ "Blender",          NULL,           "Blender Preferences",      0,     1,           0,           0,           0 },
	{ NULL,               "Godot_Engine", NULL,                  1 << 2,     0,           0,           0,           0 },
	{ NULL,               "Godot_Editor", NULL,                      15,     0,           0,           0,           0 },

	// Games
	{ "battle.net.exe",   NULL,           NULL,                  T_GAME,     1,           0,           0,           M_GAME },
	{ "overwatch.exe",    NULL,           NULL,                  T_GAME,     0,           0,           0,           M_GAME },
	{ "wow.exe",          NULL,           NULL,                  T_GAME,     0,           0,           0,           M_GAME },
	{ "wineboot.exe",     NULL,           NULL,                  T_GAME,     1,           0,           0,           M_GAME },
	{ "steam_app_526870", NULL,           NULL,                  T_GAME,     1,           0,           0,           M_GAME },
	{ "vrmonitor",        NULL,           NULL,                  T_GAME,     1,           0,           1,           M_GAME },

	// Small apps
	{ "vlc",              NULL,           NULL,                  1 << 1,     1,           0,           0,           0 },
	{ "Gnome-calculator", NULL,           NULL,                       0,     1,           0,           1,           0 },
	{ "Pavucontrol",      NULL,           NULL,                  1 << 0,     0,           0,           1,           1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-l", "10", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *roficmd[] = { "rofi", "-show", "drun", "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL }; static const char *rofipasscmd[] = { "/home/purplg/sh/rofi-pass", NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *mpctogglecmd[] = { "mpc", "toggle", NULL };
static const char *mpcnextcmd[] = { "mpc", "next", NULL };
static const char *mpcprevcmd[] = { "mpc", "prev", NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_backslash,    spawn,          {.v = mpctogglecmd } },
	{ MODKEY,                       XK_bracketright, spawn,          {.v = mpcnextcmd } },
	{ MODKEY,                       XK_bracketleft,  spawn,          {.v = mpcprevcmd } },
	{ MODKEY,                       XK_space,        spawn,          {.v = roficmd } },
	{ MODKEY,                       XK_p,            spawn,          {.v = rofipasscmd } },
	{ MODKEY,                       XK_Return,       spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,            togglebar,      {0} },
	{ MODKEY,                       XK_j,            focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,            focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,            incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,            incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,            movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,            movestack,      {.i = -1 } },
	{ MODKEY,                       XK_h,            setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,            setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,            setmfact,       {.f = -0.01} },
	{ MODKEY|ShiftMask,             XK_l,            setmfact,       {.f = +0.01} },
	{ MODKEY|ShiftMask,             XK_Return,       zoom,           {0} },
	//{ MODKEY,                       XK_Tab,          view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,            killclient,     {0} },
	{ MODKEY,                       XK_t,            setlayout,      {.v = &layouts[0]} },
	//{ MODKEY,                       XK_f,            setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_f,            setlayout,      {.v = &layouts[2]} },
	//{ MODKEY,                       XK_space,        setlayout,      {0} },
 	{ MODKEY,                       XK_u,            setlayout,      {.v = &layouts[3]} },
 	{ MODKEY,                       XK_o,            setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_t,            togglealwaysontop, {0} },
	{ MODKEY|ShiftMask,             XK_space,        togglefloating, {0} },
	{ MODKEY,                       XK_0,            view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,            tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,        focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_Tab,          focusmon,       {.i = +1 } },
	{ MODKEY,                       XK_w,            focusmon,       {.i = 0 } },
	{ MODKEY,                       XK_e,            focusmon,       {.i = 1 } },
	{ MODKEY|ShiftMask,             XK_comma,        tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Tab,          tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                            0)
	TAGKEYS(                        XK_2,                            1)
	TAGKEYS(                        XK_3,                            2)
	TAGKEYS(                        XK_4,                            3)
	TAGKEYS(                        XK_5,                            4)
	TAGKEYS(                        XK_6,                            5)
	TAGKEYS(                        XK_7,                            6)
	TAGKEYS(                        XK_8,                            7)
	TAGKEYS(                        XK_9,                            8)
	{ MODKEY|ShiftMask,             XK_q,            quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
